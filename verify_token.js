const jwt= require('jsonwebtoken');
var verify_token=(req,res,next)=>
{
    
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !==undefined)
    {
         const bearer = bearerHeader.split(' ');
         const bearertoken = bearer[1];
         req.token= bearertoken;
         jwt.verify(req.token,'ansucrm',(err,decoded)=>
    {
        console.log(err);
        if(err)
        {
            res.status(500).json({"message":"Token Mismatched or  Expired"}); 
        }
        else
        {
            next();
        }
    });
    }
    else
    {
        res.status(500).json({"message":"Error occured"});
    }
    
}

module.exports=verify_token;