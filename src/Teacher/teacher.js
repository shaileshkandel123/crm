const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Teacher= require('../../model/Teacher/teacher_model');
const verify_token= require('../../verify_token');

app.post('/create_teacher',verify_token,(req,res)=>
{
    var create_teacher = new Teacher(req.body);
    create_teacher.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        res.status(200).json({"message":"Teacher created Successfully"});

    })
})

app.post('/select_teacher',verify_token,(req,res)=>
{
Teacher.find({},(err,result)=>
{
    if(err)
    {
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({message:"OK",teacherlist:result})
});

})

app.post('/update_teacher',verify_token,(req,res)=>
{
  
    Teacher.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_teacher',verify_token,(req,res)=>
{
    console.log(req.body.id);
   Teacher.deleteOne({_id:req.body.id},(err)=>
   {
       if(err) 
    {
        console.log(err);
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({"message":"Teacher deleted Successfully"});

   });
        

    });
module.exports=app;