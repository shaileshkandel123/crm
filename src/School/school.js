const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const School= require('../../model/School/school_model');
const verify_token= require('../../verify_token');

app.post('/create_school',verify_token,(req,res)=>
{
    var create_School = new School(req.body);
    create_School.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        else
        {
        res.status(200).json({"message":"School created Successfully"});
        }

    })
})

app.post('/select_school',verify_token,(req,res)=>
{
School.find({},(err,result)=>
{
    if(err)
    {
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({message:"OK",schoollist:result})
});

})

app.post('/update_school',verify_token,(req,res)=>
{
  
    School.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_school',verify_token,(req,res)=>
{
    console.log(req.body.id);
   School.deleteOne({_id:req.body.id},(err)=>
   {
       if(err) 
    {
        console.log(err);
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({"message":"School deleted Successfully"});

   });
        

    });
module.exports=app;