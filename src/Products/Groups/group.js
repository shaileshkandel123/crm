const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Group= require('../../../model/Products/Groups/group_model');
const Item= require('../../../model/Products/Item/item_model');



const verify_token= require('../../../verify_token');

app.post('/create_group',verify_token,(req,res)=>
{
    var brand_id = Category.find({_id:req.body.id});
    var create_group = new Group({name:req.body.name,category:req.body.category,brand:brand_id[0].brand[0]});
    create_group.save((err,result)=>
    {
        if(err) 
        {
            console.log(err);
            res.status(500).json({"message":"Error occured"});
        }
        else
        {
        res.status(200).json({"message":"Group created Successfully"});
        }
    })
})

app.post('/select_group',verify_token,(req,res)=>
{
    Group.aggregate([
        {
             $lookup: {
                from:'categories',
                localField: 'category',
                foreignField: '_id',
                as: 'category'
            }
        },
        {
            $lookup: {
                from:'brands',
                localField: 'brand',
                foreignField: '_id',
                as: 'brand'
            }
        },
        ],(err,result)=>
        {
            console.log(err);
            console.log(result);   
            res.status(200).json({grouplist:result});
        
        })

})

app.post('/update_group',verify_token,(req,res)=>
{
  
    Group.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_group',verify_token,(req,res)=>
{

    var delete_group= Group.deleteOne({_id:req.body.id});
    var delete_item= Item.deleteMany({group:req.body.id});

    Promise.all([delete_group,delete_item]).then((result)=>
    {
        res.status(200).json({"message":"Deleted Successfully"});
    })
    .catch((err)=>
    {
        res.status(500).json({"message":"Error occured"});
    })


    });
module.exports=app;