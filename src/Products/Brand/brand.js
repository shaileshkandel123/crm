const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Brand= require('../../../model/Products/Brand/brand_model');
const Category= require('../../../model/Products/Category/category_model');
const Group= require('../../../model/Products/Groups/group_model');
const Item= require('../../../model/Products/Item/item_model');



const verify_token= require('../../../verify_token');

app.post('/create_brand',verify_token,(req,res)=>
{
    var create_brand = new Brand(req.body);
    create_brand.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        res.status(200).json({"message":"Brand created Successfully"});

    })
})

app.post('/select_brand',verify_token,(req,res)=>
{
Brand.find({},(err,result)=>
{
    if(err)
    {
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({message:"OK",brandlist:result})
});

})

app.post('/update_brand',verify_token,(req,res)=>
{
  
    Brand.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_brand',verify_token,(req,res)=>
{
    console.log(req.body.id);
    var delete_brand= Brand.deleteOne({_id:req.body.id});
    var delete_category= Category.deleteMany({brand:req.body.id});
    var delete_group= Group.deleteMany({brand:req.body.id});
    var delete_item= Item.deleteMany({brand:req.body.id});

    Promise.all([delete_brand,delete_category,delete_group,delete_item]).then((result)=>
    {
        res.status(200).json({"message":"Deleted Successfully"});
    })
    .catch((err)=>
    {
        res.status(500).json({"message":"Error occured"});
    })


    });
module.exports=app;