const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Item= require('../../../model/Products/Item/item_model');
const verify_token= require('../../../verify_token');

app.post('/create_item',verify_token,(req,res)=>
{
    var category_id= Group.find({_id:req.body.id});
    var brand_id = Category.find({_id:category_id[0].brand[0]});
    var create_item = new Item({name:req.body.name,group:req.body.group,category:category_id[0].category[0],brand:brand_id[0].brand[0]});
    create_item.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        else
        {
        res.status(200).json({"message":"Item created Successfully"});
        }

    })
})

app.post('/select_item',verify_token,(req,res)=>
{
    Item.aggregate([
        {
                     $lookup: {
                        from:'categories',
                        localField: 'category',
                        foreignField: '_id',
                        as: 'category'
                    }
                },
                {
                    $lookup: {
                        from:'brands',
                        localField: 'brand',
                        foreignField: '_id',
                        as: 'brand'
                    }
                },
                {
                $lookup: {
                    from:'groups',
                    localField: 'group',
                    foreignField: '_id',
                    as: 'group'
                }
        }],(err,result)=>
        {
            console.log(err);
            console.log(result);   
            res.status(200).json({grouplist:result});
        
        })

})

app.post('/update_item',verify_token,(req,res)=>
{
  
    Item.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_item',verify_token,(req,res)=>
{
    console.log(req.body.id);
   Item.deleteOne({_id:req.body.id},(err)=>
   {
       if(err) 
    {
        console.log(err);
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({"message":"Item deleted Successfully"});

   });
        

    });
module.exports=app;