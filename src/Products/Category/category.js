const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Category= require('../../../model/Products/Category/category_model');
const Brand= require('../../../model/Products/Brand/brand_model');
const Group= require('../../../model/Products/Groups/group_model');
const Item= require('../../../model/Products/Item/item_model');

const verify_token= require('../../../verify_token');

app.post('/create_category',verify_token,(req,res)=>
{
    var create_category = new Category(req.body);
    create_category.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        res.status(200).json({"message":"Category created Successfully"});

    })
})

app.post('/select_category',verify_token,(req,res)=>
{

Category.aggregate([
{
     $lookup: {
        from:'brands',
        localField: 'brand',
        foreignField: '_id',
        as: 'brand'
    }
  
}],(err,result)=>
{
   res.status(200).json({categorylist:result});
})
})

app.post('/update_category',verify_token,(req,res)=>
{
  
    Category.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_category',verify_token,(req,res)=>
{
    var delete_category= Category.deleteOne({_id:req.body.id});
    var delete_group= Group.deleteMany({category:req.body.id});
    var delete_item= Item.deleteMany({category:req.body.id});

    Promise.all([delete_category,delete_group,delete_item]).then((result)=>
    {
        res.status(200).json({"message":"Deleted Successfully"});
    })
    .catch((err)=>
    {
        res.status(500).json({"message":"Error occured"});
    })


    });
module.exports=app;