const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Deal= require('../../model/Deal/deal_model');
const verify_token= require('../../verify_token');

app.post('/create_deal',verify_token,(req,res)=>
{
    var create_deal = new Deal(req.body);
    create_deal.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        else
        {
        res.status(200).json({"message":"Deal created Successfully"});
        }

    })
})

app.post('/select_deal',verify_token,(req,res)=>
{
Deal.find({},(err,result)=>
{
    if(err)
    {
        res.status(500).json({"message":"Error occured"});
    }
    else
    {
    res.status(200).json({message:"OK",deallist:result})
    }
});

})

app.post('/update_deal',verify_token,(req,res)=>
{
  
    Deal.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
    else
    {
        res.status(200).json({"message":"Successfully Updated"});
    }
    })
})

app.post('/delete_deal',verify_token,(req,res)=>
{
    console.log(req.body.id);
   Deal.deleteOne({_id:req.body.id},(err)=>
   {
       if(err) 
    {
        console.log(err);
        res.status(500).json({"message":"Error occured"});
    }
    else
    {
    res.status(200).json({"message":"Deal deleted Successfully"});
    }

   });
        

    });
module.exports=app;