const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const UserCreate= require('../../model/Register/register_model');
app.post('/register',(req,res)=>
{
var check_user=UserCreate.find({username:req.body.username});
check_user.then((result)=>
{
    if(result.length===0)
    {
        var hash_password= new Promise((resolve,reject)=>
        {
            bcrypt.hash(req.body.password,10,(err,hash)=>{
                if(err) reject(err);
                req.body.password=hash;
                resolve("Okay");
            })
        })
        hash_password.then((result)=>
        {
            var create_user = new UserCreate(req.body);
            create_user.save(function (err, result2) {
                  if (err)
                  { 
                    res.status(500).json({"message":"Error Occured"});
                  }
                else
                {
                res.status(200).json({"message":"Added Successfully"});
                }
                  });  
        })
        
    }
    else
    {
        res.status(500).json({"message":"User already exist"})
    }
})


})


module.exports = app;