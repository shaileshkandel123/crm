const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Task= require('../../model/Task/task_model');
const verify_token= require('../../verify_token');

app.post('/create_task',verify_token,(req,res)=>
{
    var create_task = new Task(req.body);
    create_task.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        res.status(200).json({"message":"Task created Successfully"});

    })
})

app.post('/select_task',verify_token,(req,res)=>
{
Task.find({},(err,result)=>
{
    if(err)
    {
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({message:"OK",tasklist:result})
});

})

app.post('/update_task',verify_token,(req,res)=>
{
  
    Task.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_task',verify_token,(req,res)=>
{
    console.log(req.body.id);
   Task.deleteOne({_id:req.body.id},(err)=>
   {
       if(err) 
    {
        console.log(err);
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({"message":"Task deleted Successfully"});

   });
        

    });
module.exports=app;