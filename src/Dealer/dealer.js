const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Dealer= require('../../model/Dealer/dealer_model');
const verify_token= require('../../verify_token');

app.post('/create_dealer',verify_token,(req,res)=>
{
    var create_dealer = new Dealer(req.body);
    create_dealer.save((err,result)=>
    {
        if(err) 
        {
            
            res.status(500).json({"message":"Error occured"});
        }
        res.status(200).json({"message":"Dealer created Successfully"});

    })
})

app.post('/select_dealer',verify_token,(req,res)=>
{
Dealer.find({},(err,result)=>
{
    if(err)
    {
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({message:"OK",dealerlist:result})
});

})

app.post('/update_dealer',verify_token,(req,res)=>
{
  
    Dealer.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_dealer',verify_token,(req,res)=>
{
    console.log(req.body.id);
   Dealer.deleteOne({_id:req.body.id},(err)=>
   {
       if(err) 
    {
        console.log(err);
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({"message":"Dealer deleted Successfully"});

   });
        

    });
module.exports=app;