const express= require('express');
const app= express();
const bcrypt = require('bcrypt');
const jwt= require('jsonwebtoken');
const Ticket= require('../../model/Ticket/ticket_model');
const base64Img= require('base64-img');
const multer = require('multer');
const verify_token= require('../../verify_token');

var storage= multer.diskStorage({
    destination:(req,file,cb)=>
    {
        cb(null,'ticket_images/')
    },
    filename:function(req,file,cb)
    {
        cb(null,Date.now+file.originalname)
    }
})

var upload= multer({storage:storage});

app.post('/create_ticket',verify_token,upload.array('ticketimage',7),(req,res)=>
{
    var fileinfo= req.files;
    var filelist=[];
    fileinfo.forEach((filelist,index)=>
    {
        filelist.push(filelist.filename);
    })
        req.body.ticket_image_location=Array(image_location);
        var create_ticket = new Ticket(req.body);
        create_ticket.save((err,result)=>
        {
            if(err) 
            {
            res.status(500).json({"message":"Ticket couldnot saved"});
            }
            else
            {
            res.status(200).json({"message":"Ticket created Successfully",fileinfo});
            }
    
        }).catch((err)=>
{
    console.log(err);
    res.status(500).json({message:"Ticket coulndnot be saved"});
})
          
 
})

app.post('/select_ticket',verify_token,(req,res)=>
{
Ticket.find({},(err,result)=>
{
    if(err)
    {
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({message:"OK",ticketlist:result})
});

})

app.post('/update_ticket',verify_token,(req,res)=>
{
  
    Ticket.updateOne({_id:req.body.id},req.body,(err,result)=>{
        if(err)
        { res.status(500).json({"message":"Error in updating"});
    }
        res.status(200).json({"message":"Successfully Updated"});
    })
})

app.post('/delete_ticket',verify_token,(req,res)=>
{
    console.log(req.body.id);
   Ticket.deleteOne({_id:req.body.id},(err)=>
   {
       if(err) 
    {
        console.log(err);
        res.status(500).json({"message":"Error occured"});
    }
    res.status(200).json({"message":"Ticket deleted Successfully"});

   });
        

    });
module.exports=app;