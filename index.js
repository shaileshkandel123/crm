const express= require('express');
const bodyParser= require('body-parser');
const cookieParser= require('cookie-parser');
const bcrypt= require('bcrypt');
const mongoose = require('mongoose');
const jwt= require('jsonwebtoken');
const verify_token= require('./verify_token');
const app= express();
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
     limit: '50mb' ,
    extended:true
}));

//Required routes
app.use('/',require('./src/Register/register'));
app.use('/',require('./src/Login/login'));
app.use('/',require('./src/Dealer/dealer'));
app.use('/',require('./src/School/school'));
app.use('/',require('./src/Products/Brand/brand'));
app.use('/',require('./src/Products/Category/category'));
app.use('/',require('./src/Products/Groups/group'));
app.use('/',require('./src/Products/Item/item'));
app.use('/',require('./src/Teacher/teacher'));
app.use('/',require('./src/Ticket/ticket'));
app.use('/',require('./src/Deal/deal'));
app.use('/',require('./src/Ticket/ticket'));

//MongoDB connection using cloud.mongodb.com
mongoose.connect("mongodb+srv://ansucrm:ansucrm@cluster0-6tzhv.mongodb.net/test?retryWrites=true&w=majority",{useNewUrlParser:true,useUnifiedTopology:true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
 console.log("Database connected");
});


app.get('/hello',verify_token,(req,res)=>
{
    res.status(200).json({"message":"jsonwebtoken"})
})

const port = 3010;
app.listen(port,()=>{
    console.log(`Listening on port ${port}`);
})