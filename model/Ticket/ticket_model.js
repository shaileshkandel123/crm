
  const mongoose = require('mongoose');
  var ticket = new mongoose.Schema({
      name:{type:String,required:true},
      pipeline:{type:String,enum:["Sales","Support"]},
      status:{type:String,enum:["S","NS"],default:"NS"},
      description:{type:String},
      source:{type:String},
      priority:{type:String,enum:["High","Medium","Low"]},
      associated_company:{type:String},
      company_contact:{type:String},
      ticket_image_location:[String]
  });
  module.exports=mongoose.model("Ticket",ticket);