
  const mongoose = require('mongoose');
  var school = new mongoose.Schema({
    name: {type:String,required:true},
    principalname:{type:String,required:true},
    phonenumber:{type:String},
    landlinenumber:{type:String},
    alternatenumber:{type:String},
    state:{type:String},
    district:{type:String},
    municapality:{type:String},
    wardno:{type:Number},
    postalcode:{type:Number},
    numberofstudent:{type:Number},
    description:{type:String},
    image:{type:Buffer},
    createdAt:{type:Date,default:Date.now}
    });

module.exports = mongoose.model('School',school);

    