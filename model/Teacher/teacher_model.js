  const mongoose = require('mongoose');
  var teacher = new mongoose.Schema({
    name: {type:String,required:true},
    mobileno:{type:String,required:true},
    email:{type:String,required:true},
    subject:{type:Number,required:true},
    description:{type:String},
    image:{type:Buffer},
    assignedSchool:{type:mongoose.Schema.Types.ObjectId,ref:'schools',required:true},
    createdAt:{type:Date,default:Date.now}
    });

module.exports = mongoose.model('Teacher',teacher);

    