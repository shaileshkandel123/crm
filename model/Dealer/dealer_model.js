
  const mongoose = require('mongoose');
  var dealer = new mongoose.Schema({
    name: {type:String,required:true},
    ownername:{type:String,required:true},
    industry:{type:String},
    phonenumber:{type:String,required:true},
    state:{type:String},
    district:{type:String},
    municapality:{type:String},
    wardno:{type:Number},
    postalcode:{type:Number},
    numberofemployee:{type:Number},
    description:{type:String},
    createdAt:{type:Date,default:Date.now}
    });

module.exports = mongoose.model('Dealer',dealer);

    