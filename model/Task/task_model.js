const mongoose= require('mongoose');
const task = mongoose.Schema({
type:{type:String},
priority:{type:String},
assigned_to:{type:String},
queue:{type:String},
due_date:{type:Date},
notes:{type:String},
associated_record_with:{type:String},
due_time:{type:Date,default:Date.now}
});

module.exports= mongoose.model('Task',task);