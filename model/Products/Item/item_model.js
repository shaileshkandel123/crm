const mongoose = require('mongoose');
var item = new mongoose.Schema({
    name:{type:String,required:true,index: { unique: true }},
    price:{type:Number,required:true},
    unit:{type:String,required:true},
    status:{type:String},
    warrantyStatus:{type:String,enum:["Yes","No"]},
    warranty:{type:String},
    specification:[{}],
    category:[{type:mongoose.Schema.Types.ObjectId,ref:'categories'}],
    brand:[{type:mongoose.Schema.Types.ObjectId,ref:'brands'}],
    group:[{type:mongoose.Schema.Types.ObjectId,ref:'groups'}]
});

module.exports= mongoose.model('Item',item);