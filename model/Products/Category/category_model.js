  const mongoose = require('mongoose');
  var category = new mongoose.Schema({
      name:{type:String,required:true,index: { unique: true }},
      brand:[{type:mongoose.Schema.Types.ObjectId,ref:'brands'}]
  });

  module.exports=mongoose.model('Category',category);