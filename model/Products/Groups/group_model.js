const mongoose = require('mongoose');
var group = new mongoose.Schema({
    name:{type:String,required:true,index: { unique: true }},
    category:[{type:mongoose.Schema.Types.ObjectId,ref:'categories'}],
    brand:[{type:mongoose.Schema.Types.ObjectId,ref:'brands'}]

});

module.exports=mongoose.model('Group',group);