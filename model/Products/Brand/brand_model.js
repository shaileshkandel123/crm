const mongoose = require('mongoose');
var brand = new mongoose.Schema({
    name:{type:String,required:true,index: { unique: true }}
});

module.exports=mongoose.model('Brand',brand);