

  const mongoose = require('mongoose');
  const deal= mongoose.Schema({
      owner:{type:String,required:true},
      pipeline:{type:String,required:true},
      deal_stage:{type:String,required:true},
      amount:{type:String,required:true},
      close_data:{type:Date},
      deal_type:{type:String}
  })

  module.exporta= mongoose.model('Deal',deal);